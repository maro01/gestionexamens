﻿<?php
session_start();
if(isset($_SESSION['login']))
{
 header("location:admin_principale.php");
 exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Connexion</title>
    <link rel="stylesheet" href="">
    <style>
        body{
     margin: 0;
   padding : 0;
  background : #ffffff;
}

h1
     {
         color : rgb(57,121,218);
     }  
.c img
{
  position : absolute ;
  width:291px;
  left:523px;
 top: 7%;
}
/********************/
.login-box
{
    width: 280px;
    position: absolute;
    top:16%;
    left: 50%;
    transform: translate(-50%,50%);
    color: white;
    font-family: sans-serif;
}
.login-box h1{
    float: left;
    font-size:40px ;
    border-bottom:6px solid#000000c2;
    margin-bottom: 50px;
    padding: 0px 0;
}
.textbox{
    width: 100%;
    overflow: hidden;
    font-size: 20px;
    padding: 8px 0;
    margin:8px 0;
    border-bottom: 1px solid #000000c2  ;
}
.textbox img{
    width: 26px;
    float: left;
    text-align: center;
}
.textbox input{
     border: none;
     outline: none;
     background: none;
     color: #1f0202;
     font-size:18px;
     width: 180px;
     float: left;
     margin: 10px;
}

.btn
{
    width: 100%;
    background: none;
    border: 1px solid #000000c2;
    color: rgb(57,121,218);
    font-size: 24px;
    cursor: pointer;
    margin: 12px 0;
}
input[type="submit"]:hover
{
	width: 100%;
    background: rgb(57,121,218);
    border: 1px solid white;
    color: white;
    font-size: 24px;
    cursor: pointer;
    margin: 12px 0;
}

	.menu img
{
    width: 50px;
    height: 60px;
    float: left;
    position: absolute;
    left: 41%;
    top: 18%;
}
.menu
{

	width: 100%;
	height: 76px;
	background-color: #fff;
	position: fixed;
	margin-top : -1%;
}

.scolarite
{
    font-family: 'Hind Vadodara',sans-serif;
    letter-spacing: 2px;
    position: absolute;
    left: 46%;
    top: 33%;
    font-size:37px ;
    text-decoration: none;
    color: #1e1e1e;
}
input[type="image"].return
{
	height:50px;
	width:50x;
	margin: 0px 0px 0px 0px;
}
    </style>

</head>
<body>
    <header>
        <nav class="menu">
            <a href="../index.php" class="scolarite">Scolarité</a>
            <img src="ump.png" alt="">

        </nav>
    </header>
    <div class="c">
        <img src="conn1.png">
    </div>

    <div class="login-box">

        <h1>Se connecter</h1>
		 <form method="POST" action="connexion_admin_checking.php" class="form-inline" >
            <div class="textbox">
                <img src="connexion_user.png" alt="">
                <input type="text" placeholder="Nom d'utilisateur" name="user" id="user">
            </div>
            <div class="textbox">
                <img src="lock.png" alt="">
                <input type="password" placeholder="Mot de passe" name="password" id="password">
            </div>
            <input class="btn" type="submit" name="" value="Se connecter">
        </form>
<?php
if(isset($_REQUEST['erreur']))
{
    echo "<h4 style='color:red;font-family:tahoma;'>Nom d'utilisateur ou mot de passe sont incorrectes</h4>";
	 unset($_REQUEST['erreur']);
}
?>
    </div>
</body>
</html>