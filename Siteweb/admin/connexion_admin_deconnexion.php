<?php
session_start();
session_destroy();
if(!isset($_SESSION['id_admin']))
unset($_SESSION['id_admin']);

if(!isset($_SESSION['nom_utilisateur']))
unset($_SESSION['nom_utilisateur']);

if(!isset($_SESSION['motdepasse']))
unset($_SESSION['motdepasse']);

if(!isset($_SESSION['access']))
unset($_SESSION['access']);

if(!isset($_SESSION['login']))
unset($_SESSION['login']);

header("location:connexion_admin.php");
?>