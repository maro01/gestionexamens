<?php
session_start();
if(!isset($_SESSION['login']))
{
include 'connexion_admin_deconnexion.php';
header("location:../connexion_admin.php");
exit;
}
?>
<!DOCTYPE html>
<head>
<meta CHARSET="UTF-8">
<link rel="stylesheet" type="text/css">
<style>
    div.copyright
{
	text-align:right;
	margin-top:125px;
}
h4
{
	font-family:tahoma;
}
table.media
{
	float:left;
	margin-top:105px;
	border-spacing:20px;
}
input[type=text]{

  width: 100%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}

select
{
  margin-right:88px;
  width: 80%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}
input[type=time]
{
  margin-right:88px;
  width: 80%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}
input[type=date]
{
  margin-right:88px;
  width: 80%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}
table#boutons
{
    margin-left:auto;
	margin-right:auto;
}
input[type="submit"]
{
	margin: 10px 0px 0px 0px;
	font-family:tahoma;

}
input[type="button"]
{
	margin: 10px 0px 0px 0px;
	font-family:tahoma;

}
.postionate
{
	text-align:left;
	float:left;
}
pre
{
	font-size:12px;
}
table.inscription
{
	margin-left:auto;
	margin-right:auto;

}

img.logo
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  height:200px;
  width:300px;
}
input[type="submit"]
{
color: rgb(91, 161, 212);
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
input[type="button"]
{
color: rgb(91, 161, 212);
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
div > div.notice
{
	text-align:center;
	font-family:arial;
	color:rgb(227, 230, 228);

}

div.bleu
{
    background-color:rgb(165, 194, 237);
	width:100%;
	height:300px;
	color:white;
	font-family:tahoma;
	font-size:15px;
	text-align:center;
	padding:20px 0px;
	font-weight:bold;
}
span
{
	white-space: nowrap;
}
div.position
{
	margin-right:400px;
}
a.c1
{
  background-color:#6473ed;
  border:2px solid #6473ed;
  color:white;
  padding: 5px 5px;
  text-decoration:none;
  font-family:tahoma;
  margin-left:85%;
}

#dialogoverlay{
	display: none;
	opacity: .8;
	position: fixed;
	top: 0px;
	left: 0px;
	background:#FFF;
	width: 100%;
	z-index: 10;
}
#dialogbox{
	display: none;
	position: fixed;
	width:400px; 
	z-index: 10;
    border-style:inset;
    border-color:rgb(255,51,51);
}

#dialogboxhead{ background: #f2f2f2; font-size:19px; padding:10px; font-family:tahoma;color:rgb(141,141,141); }
#dialogboxbody{ background:white; padding:20px; color:black;font-family:tahoma; }
#dialogboxfoot{ background: #f2f2f2; padding:10px; text-align:center; }


         .btn {
            background-color :rgb(255,51,51);
            border: none;
            color: white;
            padding: 5px 5px;
			margin-right:10px;
            text-align: center;
			font-size:20px
			border-radius:10px;
            text-decoration: none;
            display: inline-block;
            font-family: 'Hind Vadodara',sans-serif;
			}
img
{
	width:20px;
	height:15px;
}
img.alert
{
	width:50px;
	height:50px;
}
</style>
<script>
function startw(date)
  {
    var diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1);
  
    return new Date(date.setDate(diff));
 
  } 
function get(name){
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}
function myfunction3()
{
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET","ajax3.php?etab="+document.getElementById('etab').value+"&niveau="+document.getElementById('niveau').value,false);
	xmlhttp.send(null);
	document.getElementById('module').innerHTML=xmlhttp.responseText;
}
function myfunction1()
{
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET","ajax1.php?etab="+document.getElementById('etab').value+"&niveau="+document.getElementById('niveau').value,false);
	xmlhttp.send(null);
	document.getElementById('filiere').innerHTML=xmlhttp.responseText;
	myfunction3();
}
function myfunction2()
{
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET","ajax2.php?etab="+document.getElementById('etab_rat').value,false);
	xmlhttp.send(null);
	document.getElementById('salle').innerHTML=xmlhttp.responseText;
}

function myfunction()
{
	dating();
	myfunction1();
	myfunction2();
	groupeChecker();
}
function checking()
{
  	    var x = new Date(document.getElementById('date').value);
	    x = new Date(x).toISOString().slice(0, 10);
		
		var result = new Date();
		result.setDate(result.getDate());
		var result = startw(result);
		let day = new Date(result).toISOString().slice(0, 10);

  
	if (day>x) {
    alert('Le calendrier commence du : '+day);
	event.preventDefault();
  }
}

function groupeChecker()
{
	if(document.getElementById('nogroupe').checked) document.getElementById('groupe').hidden =true;
		
	else   document.getElementById('groupe').hidden =false;

}
function dating()
{
		var result1 = new Date();
		var result2 = new Date();
		var result3 = new Date();
		var result4 = new Date();
		
		
		result1.setDate(result1.getDate());
		var result1 = startw(result1);
		curr1=result1;
		let first1 = curr1.getDate()+ - curr1.getDay()+1;
        let day1 = new Date(result1.setDate(first1)).toISOString().slice(0, 10);

		
		result2.setDate(result2.getDate()+7);
		var result2 = startw(result2);
		curr2=result2;
		let first2 = curr2.getDate()+ - curr2.getDay()+1;
        let day2 = new Date(result2.setDate(first2)).toISOString().slice(0, 10);

		
		result3.setDate(result3.getDate()+14);
		var result3 = startw(result3);
		curr3=result3;
		let first3 = curr3.getDate()+ - curr3.getDay()+1;
        let day3 = new Date(result3.setDate(first3)).toISOString().slice(0, 10);

		
	    result4.setDate(result4.getDate()+21);
		var result4 = startw(result4);
		curr4=result4;
		let first4 = curr4.getDate()+ - curr4.getDay()+1;
        let day4 = new Date(result4.setDate(first4)).toISOString().slice(0, 10);
		
		
	
	     if(get('dating')==1) document.getElementById('date').value=day1;
	else if(get('dating')==2) document.getElementById('date').value=day2;
	else if(get('dating')==3) document.getElementById('date').value=day3;
	else if(get('dating')==4) document.getElementById('date').value=day4;
}
function customAlert()
{	
	this.render1=function(){
		var winW=window.innerWidth;
		var winH=window.innerHeight;
		var dialogoverlay=document.getElementById('dialogoverlay');
		var dialogbox=document.getElementById('dialogbox');
		var head=document.getElementById('dialogboxhead');
		var body=document.getElementById('dialogboxbody');
		var foot=document.getElementById('dialogboxfoot');
		dialogoverlay.style.display='block';
		dialogoverlay.style.height=winH+"px";
		dialogbox.style.left="34%";
		dialogbox.style.top="30%";
		dialogbox.style.display="block";
		head.innerHTML="Alerte";
		body.innerHTML='<table><tr><td><img class="alert" src="alert.png"></td><td>La date entrée doit être à partir d\'<b>aujourd\'hui</b></td></tr></table>';
		foot.innerHTML="<button href='#' onclick='Alert.cancel()' class='btn'>Ok</button>";
	}
		this.render2=function(dt){
		var winW=window.innerWidth;
		var winH=window.innerHeight;
		var dialogoverlay=document.getElementById('dialogoverlay');
		var dialogbox=document.getElementById('dialogbox');
		var head=document.getElementById('dialogboxhead');
		var body=document.getElementById('dialogboxbody');
		var foot=document.getElementById('dialogboxfoot');
		dialogoverlay.style.display='block';
		dialogoverlay.style.height=winH+"px";
		dialogbox.style.left="34%";
		dialogbox.style.top="30%";
		dialogbox.style.display="block";
		head.innerHTML="Alerte";
		body.innerHTML='<table><tr><td><img class="alert" src="alert.png"></td><td>La date ne doit pas dépasser le <br><b> '+dt+' </b></td></tr></table>';
		foot.innerHTML="<button href='#' onclick='Alert.cancel()' class='btn'>Ok</button>";
	}
	
	this.cancel=function(){
		document.getElementById('dialogoverlay').style.display='none';
		document.getElementById('dialogbox').style.display='none';
		return false;
	}
}
function checking()
{
  	    var x = new Date(document.getElementById('date').value);
	    x = new Date(x).toISOString().slice(0, 10);
		
		var result = new Date();
		result.setDate(result.getDate());
		let day = new Date(result).toISOString().slice(0, 10);
		
		var result = new Date();
		result.setDate(result.getDate()+21);
		var result = startw(result);
		result.setDate(result.getDate()+5);
		let end = new Date(result).toISOString().slice(0, 10);


	if (x<day) {
	Alert.render1();
	event.preventDefault();
  }
  else if(x>end)
  {
	 
	Alert.render2(end);
	event.preventDefault();
  }
}
var Alert = new customAlert();
</script>
<link rel="icon" href="../ump.png" type="image/x-icon" />
<title>Scolarité</title>
</head>
<body onload="myfunction()">
<div id="dialogoverlay"></div>
<div id="dialogbox">
<div>
<div id="dialogboxhead"></div>
<div id="dialogboxbody">
</div>
<div id="dialogboxfoot"></div>
</div>
</div>
<img class="logo" src="../ump.png">
<a class="c1" href="modif/choix1/principale1.php">Voir le calendrier</a>
<br>
<form name="formulaire" method="post" enctype="multipart/form-data" onsubmit="checking()">
<div class="bleu">
<br>
<table class="inscription">
<tr>
<td>
<table>
<td align="right">
<label for="etab" class="postionate" >Etablissement</label>
</td>
<td align="left">

<select name="etab" id="etab" class="select" onchange="myfunction1()">
<optgroup label="Choisissez un établissement">
<?php
$con=mysqli_connect("localhost","root","","gestionds");
if($con)
{ 
	$result=mysqli_query($con,'select * from etablissement');
	if($result)
	{
		while($row=mysqli_fetch_assoc($result))
		{  
			echo "<option value='".$row["id_etab"]."'>".$row["libelle_etab"]."</option>";
		}
	}
}
mysqli_close($con);
?>
</optgroup>
</select><br>
</td>
</tr>
<tr>
<td align="right">
<label for="niveau" class="postionate" >Niveau universitaire</label>
</td>
<td align="left">

<select name="niveau" id="niveau" class="select" onchange="myfunction1()">
<optgroup label="Choisissez l'année universitaire">
<?php
$con=mysqli_connect("localhost","root","","gestionds");
if($con)
{ 
	$result=mysqli_query($con,'select * from niveau');
	if($result)
	{
		while($row=mysqli_fetch_assoc($result))
		{  
			echo "<option value='".$row["id_niveau"]."'>".$row["libelle_niveau"]."</option>";
		}
	}
}
mysqli_close($con);
?>
</optgroup>
</select><br>
</td>
</tr>
<tr>
<td align="right">
<label for="filiere" class="postionate">Filière</label>
</td>
<td align="left">
<select name="filiere" id="filiere" class="select" onchange="myfunction3()">
<optgroup label="Choisissez la filière universitaire">
</optgroup>
</select><br>
</td>
</tr>
<tr>
<td>
<label for="time" class="postionate">Groupe</label>
</td>
<td align="left" >
<select name="groupe" id="groupe" class="select">
<optgroup label="Choisissez le nom du groupe">
<?php
$con=mysqli_connect("localhost","root","","gestionds");
if($con)
{ 
	$result=mysqli_query($con,'select * from groupe');
	if($result)
	{
		while($row=mysqli_fetch_assoc($result))
		{  
			echo "<option value='".$row["id_groupe"]."'>".$row["libelle_groupe"]."</option>";
		}
	}
}
mysqli_close($con);
?>
</optgroup>
</select>
Pas de groupes <input type="checkbox" id="nogroupe" onclick="groupeChecker()" >
</td>
</tr>
</table>
</td>
<td>
<table>
<tr>
<td>
<label for="date"  class="postionate" >Date d'examen</label>
</td>
<td align="left" size="50px">
<input type="date"  style='width:168px' name="date" id="date"  required><br>
</td>
</tr>
<tr>
<td>
<label for="time" class="postionate">Horaire</label>
</td>
<td align="left" >
<select name="time" id="time" class="select">
<optgroup label="Choisissez l'horaire d'examen">
<option value="1">08-11 AM</option>
<option value="2">11-02 AM</option>
<option value="3">02-05 AM</option>
</optgroup>
</select><br>
</td>
</tr>
<tr>
<td align="left">
<label for="module" class="postionate">Module</label>
</td>
<td align="left">
<select name="module" id="module" class="select">
<optgroup label="Choisissez un module">
</optgroup>
</select><br>
</td>
</tr>
<tr>
<td align="right">
<label for="etab_rat" class="postionate" >Etablissement d'examen</label>
</td>
<td align="left">
<select name="etab_rat" id="etab_rat" class="select" onchange="myfunction2()" >
<optgroup label="Choisissez un établissement">
<?php
$con=mysqli_connect("localhost","root","","gestionds");
if($con)
{ 
	$result=mysqli_query($con,'select * from etablissement_rat');
	if($result)
	{
		while($row=mysqli_fetch_assoc($result))
		{  
			echo "<option value='".$row["id_etab_rat"]."'>".$row["libelle_etab_rat"]."</option>";
		}
	}
}
mysqli_close($con);
?>
</optgroup>
</select><br>
</td>
</tr>
<tr>
<td>
<label for="salle" class="postionate">Salle d'examen*</label>
</td>
<td align="left">
<select name="salle" id="salle" >
<optgroup label="Choisissez la salle d'examen">
<?php
$con=mysqli_connect("localhost","root","","gestionds");
if($con)
{
	$result=mysqli_query($con,'select * from salle');
	if($result)
	{
		while($row=mysqli_fetch_assoc($result))
			echo "<option value='".$row["id_salle"]."'>".$row["libelle_salle"]." (".$row["capacite"].")</option>";
	}
}
mysqli_close($con);
?>
</optgroup>
</select>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<table class="inscription">
<tr>
<td>
<input type="button" name="retourner" value="Retourner"  onclick="history.replaceState(null,null,'examen_principale.php');location.reload();">
</td>
<td>
<input type="submit" name="submit" value="Valider" >
</td>
</tr>
</table>
<?php include 'ajout_data.php'; ?>
<table class="media" align="left">
<tr>
<td><A href="https://www.youtube.com/channel/UCyFXZbYLiKOkthY26bCPn1Q"><img  src="youtube.jpg" height="15px" width="15px"></A></td>
<td><A href="https://www.facebook.com/UniversityOUJDA/"><img  src="facebook.jpg" height="15px" width="15px"></A></td>
<td><A href="http://esto.ump.ma/"><img  src="esto.jpg" height="15px" width="15px"></A></td>
</tr>
</table>
<div class="copyright">Copyright © 2020 Université Mohammed Premier Oujda</div>
</body>
</html>