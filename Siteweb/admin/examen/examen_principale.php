<?php
session_start();
if(!isset($_SESSION['login']))
{
include 'connexion_admin_deconnexion.php';
header("location:../connexion_admin.php");
exit;
}
?>
<!DOCTYPE html>
<head>
<meta CHARSET="UTF-8">
<link rel="stylesheet" type="text/css" >
<style>
        
.espace
{
    width:18% ;
    height: 40%;
    background: #1e1e1ebd;
    position: absolute;
	border-radius:30px;
    top: 21%;
    left: 82%; 
}
.menu img
{
    width: 50px;
    height: 60px;
    float: left;
    position: absolute;
    left: 41%;
    top: 18%;
}
.menu
{

	width: 100%;
	height: 76px;
	background-color: #fff;
	position: fixed;
	margin-top : -1%;
}
.scolarite
{
    font-family: 'Hind Vadodara',sans-serif;
    letter-spacing: 2px;
    position: absolute;
    left: 46%;
    top: 33%;
    font-size:30px ;
    text-decoration: none;
    color: #1e1e1e;
}
.button {
  background-color: #4291a2bd;
  border: none;
  color: white;
  padding: 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 26px;
}

.button:hover {
  background-color: #34717f;
  border: none;
  color: white;
  padding: 20px;
  opacity: 0.9;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 26px;
}

.button1 {border-radius: 30px;}
.button2 {border-radius: 30px;}


input[type="submit"]
{
  background-color: white; 
  color: black; 
  border: 2px solid #008CBA;
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:40px;
  width:120px;
  margin-top:28px;
  margin-right:28px;

}

input[type="button"]
{
  background-color: white; 
  color: black; 
  border: 2px solid #008CBA;
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:40px;
  width:120px;
  margin-top:28px;
  margin-right:28px;

}

input[type="submit"]:hover
{
 background-color: #008CBA;
  color: white;
}
  *
{
    margin:0;
    padding:0;
   
  }
        body{
              background:url(../background.jpg);
              background-size:cover;
              background-position:linear;   
              }

</style>
<link rel="icon" href="../ump.png" type="image/x-icon" />
<title>École Supérieure de Technologie</title>
</head>
<body>
    <header>
        <nav class="menu">
            <a href="../admin_principale.php" class="scolarite">Scolarité</a>
            <img src="../ump.png" alt="">
        </nav>
    </header>

<button   style="margin-left:360px;  margin-top: 350px;" type="button" class="button button1" href="#" onclick='history.replaceState(null,null,"ajout_index.php");location.reload();'>Ajouter un examen</button>
<button  style="margin-left:155px;  margin-top: 290px;"  type="button" class="button button2" href="#" onclick='history.replaceState(null,null,"modif/choix1/principale1.php");location.reload();'>Voir/Modifier les examens</button>

 
</body>
</html>