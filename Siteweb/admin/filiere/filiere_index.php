<?php
session_start();
if(!isset($_SESSION['login']))
{
include 'connexion_admin_deconnexion.php';
header("location:../connexion_admin.php");
exit;
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>filiere </title>
    <style>
	input[type="image"]
{
	display:inline;
	margin-top:1%;
	margin-right:94%;
	margin-left:2%;
	width:50px;
	height:50px;
}
        img.logo
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  height:100px;
  width:150px;
}
        table {
                   width: 96%;
                   margin:1% 2% 0% 2%;
                   border-width:5px;  
                   border-style:inset;
                   border-color:cornflowerblue;
              }
        th, td {
            padding: 5px;
  text-align: left;
    border-bottom: 1px solid #ddd;
    text-align:center;
}
        tr:hover {background-color :lightsteelblue}
        tr:nth-child(even) {background-color: #f2f2f2;}
         .button {
            background-color :dodgerblue;
            border: none;
            color: white;
            padding: 15px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size:20px;
            margin: 10px 1px;
            position:relative;
            top: 40px;
            left: 600px;
            right: 4px;
          
            font-family: 'Hind Vadodara',sans-serif;}
       
	   .scolarite
{
    font-family: 'Hind Vadodara',sans-serif;
    letter-spacing: 2px;
    position: absolute;
    left: 46%;
    top: 33%;
    font-size:37px ;
    text-decoration: none;
    color: #1e1e1e;
}
      *
{
    margin:0;
    padding:0;
   
  }

	.menu img
{
    width: 50px;
    height: 60px;
    float: left;
    position: absolute;
    left: 41%;
    top: 18%;
}
.menu
{

	width: 100%;
	height: 76px;
	background-color: #fff;
	position: fixed;
	margin-top : -1%;
}
        .deco
{
   font-family: 'Hind Vadodara',sans-serif;
    letter-spacing: 2px;
    position: absolute;
    left: 91%;
    top: 52%;
    font-size:12px ;
    text-decoration: none;
    color: #1e1e1e;
  
}
select
{
  margin-right:88px;
  width: 20%;
  height:40px;
  padding: 0px 5px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}
a.cc
{
  display:inline;
  background-color:white;
  border:2px solid cornflowerblue;
  color:cornflowerblue;
  padding: 0px 5px;
  margin-bottom:5px;
  text-decoration:none;
  font-family:tahoma;
  border-radius:10px;
}
a.cc:hover
{
  display:inline;
  background-color:cornflowerblue;
  border:2px solid cornflowerblue;
  color:white;
  padding: 0px 5px;
  margin-bottom:5px;
  text-decoration:none;
  font-family:tahoma;
  border-radius:10px;
}
#dialogoverlay{
	display: none;
	opacity: .8;
	position: fixed;
	top: 0px;
	left: 0px;
	background:#FFF;
	width: 100%;
	z-index: 10;
}
#dialogbox{
	display: none;
	position: fixed;
	width:400px; 
	z-index: 10;
    border-style:inset;
    border-color:cornflowerblue;
}

#dialogboxhead{ background: #f2f2f2; font-size:19px; padding:10px; font-family:tahoma;color:rgb(141,141,141); }
#dialogboxbody{ background:white; padding:20px; color:black;font-family:tahoma; }
#dialogboxfoot{ background: #f2f2f2; padding:10px; text-align:center; }


         .btn {
            background-color :dodgerblue;
            border: none;
            color: white;
            padding: 5px 5px;
			margin-right:10px;
            text-align: center;
			font-size:20px
			border-radius:10px;
            text-decoration: none;
            display: inline-block;
            font-family: 'Hind Vadodara',sans-serif;
			}
    </style>
	<link rel="icon" href="../ump.png" type="image/x-icon" />
	<script type='text/javascript' src='jquery-1.9.1.js'></script>
	<script>
function checkDelete()
{
  if(confirm('Vous êtes sûr de cette suppression ?'))return true;
  else return false;
}
function myetab()
{
	$.ajax({
    url : "ajax_ftable1.php",
    type: "GET",
	data: "etab="+$('#etab').val(),
    success: function(data)
    {
         $("#content").html(data);
    }
    });
}	
function myniveau()
{
	$.ajax({
    url : "ajax_ftable2.php",
    type: "GET",
	data: "etab="+$('#etab').val()+"&niveau="+$('#niveau').val(),
    success: function(data)
    {
         $("#content").html(data);
    }
    });
}
</script>
</head>
<body onload="myetab()">

<div id="dialogoverlay"></div>
<div id="dialogbox">
<div>
<div id="dialogboxhead"></div>
<div id="dialogboxbody"></div>
<div id="dialogboxfoot"></div>
</div>
</div>

	<header>
        <nav class="menu">
            <a href="../admin_principale.php" class="scolarite">Scolarité</a>
            <img src="../ump.png" alt="">
            <a href="../connexion_admin_deconnexion.php" class="deco">Se déconnecter</a>
        </nav>
    </header>
     
<br><br><br><br>	
<?php
if(isset($_REQUEST['ajouter']))
{
    echo "<h4 style='margin-left:5%;color:green;font-family:tahoma;'>Ajout réussi</h4>";
	 unset($_REQUEST['erreur']);
}
?><br>
<?php
if(isset($_REQUEST['erreur']))
{
    echo "<h4 style='margin-left:5%;color:red;font-family:tahoma;'>Erreur: modification erronée</h4>";
	 unset($_REQUEST['erreur']);
}
?><br>
<?php
if(isset($_REQUEST['supprimer']))
{
    echo "<h4 style='margin-left:5%;color:green;font-family:tahoma;'>Suppression réussie</h4>";
	 unset($_REQUEST['erreur']);
}
?><br>
<?php
if(isset($_REQUEST['modifier']))
{
    echo "<h4 style='margin-left:5%;color:green;font-family:tahoma;'>Modification réussie</h4>";
	 unset($_REQUEST['erreur']);
}
?><br><br>
<select id="etab" name="etab" onchange="myetab()">
<optgroup label="Choisissez un établissement">
<?php
$con=mysqli_connect("localhost","root","","gestionds");
if($con)
{ 
	$result=mysqli_query($con,'select * from etablissement');
	if($result)
	{
		while($row=mysqli_fetch_assoc($result))
		{  
			echo "<option value='".$row["id_etab"]."'>".$row["libelle_etab"]."</option>";
		}
	}
}
mysqli_close($con);
?>
</optgroup>
</select>
	
<select id="niveau" name="niveau" onchange="myniveau()">
	<optgroup label="Choisissez un niveau" >
	<option value="">Défault</option>
<?php
$con=mysqli_connect("localhost","root","","gestionds");
if($con)
{ 
	$result=mysqli_query($con,'select * from niveau');
	if($result)
	{
		while($row=mysqli_fetch_assoc($result))
		{  
			echo "<option value='".$row["id_niveau"]."'>".$row["libelle_niveau"]."</option>";
		}
	}
}
mysqli_close($con);
?>
</optgroup>
</select>
<br><br><br>
<div id="content"></div>
<a href="ajouter_index.php" class="button" >Ajouter</a>
</body>
</html>
