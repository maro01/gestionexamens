<?php
session_start();
if(!isset($_SESSION['login']))
{
include 'connexion_admin_deconnexion.php';
header("location:../connexion_admin.php");
exit;
}
?>
<!DOCTYPE html>
<head>
<meta CHARSET="UTF-8">

<link rel="stylesheet" type="text/css" >
<style>
input[type=text].ord{

  width: 100%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}
input[type=text].cap{
  width: 15%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}
div.copyright
{
	text-align:right;
	margin-top:125px;
}
h4
{
	
	font-family:tahoma;
}
table.media
{
	float:left;
	margin-top:105px;
	border-spacing:20px;
}
table#boutons
{
    margin-left:auto;
	margin-right:auto;
}
input[type="submit"]
{
	margin: 10px 0px 0px 0px;
	font-family:tahoma;

}
input[type="button"]
{
	margin: 10px 0px 0px 0px;
	font-family:tahoma;

}
.postionate
{
	text-align:left;
	float:left;
}
pre
{
	font-size:12px;
}
table.content
{
	margin-left:auto;
	margin-right:auto;
}

img.logo
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  height:200px;
  width:300px;
}
input[type="submit"]
{
color: rgb(91, 161, 212);
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
input[type="button"]
{
color: rgb(91, 161, 212);
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
div > div.notice
{
	text-align:center;
	font-family:arial;
	color:rgb(227, 230, 228);
}

div.bleu
{
	    background-color:rgb(165, 194, 237);
	width:100%;
	height:140px;
	color:white;
	font-family:tahoma;
	font-size:15px;
	text-align:center;
	padding:20px 0px;
	font-weight:bold;
}

span
{
	white-space: nowrap;
}
div.position
{
	margin-right:400px;
}
select
{
  margin-right:88px;
  width: 100%;
  height:40px;
  padding: 0px 5px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}
</style>
<link rel="icon" href="ump.png" type="image/x-icon" />
<title>École Supérieure de Technologie</title>
</head>
<body>
<form name="formulaire" method="post" enctype="multipart/form-data">

<img  class="logo" src="../ump.png">

<div class="bleu">
<table class="content">
<tr>
<td>
<label for="etab_rat" class="postionate">Etablissement d'examen</label>
</td>
<td>
<select id="etab_rat" name="etab_rat">
<optgroup label="Choisissez un etablissement d'examen">
<?php
$con=mysqli_connect("localhost","root","","gestionds");
if($con)
{ 
	$result=mysqli_query($con,'select * from etablissement_rat');
	if($result || mysqli_num_rows($result)>0)
	{  
		while($row=mysqli_fetch_assoc($result))
		{  
			echo "<option value='".$row["id_etab_rat"]."'>".$row["libelle_etab_rat"]."</option>";
		}
	}
mysqli_close($con);
}
?>
</optgroup> 
</select>
</td>
</tr>
<tr>
<td>
<label for="capacite" class="postionate">Capacité de salle</label>
</td>
<td align="left">
<input type="text" size="10px" id="capacite" name="capacite" class="cap" maxlength="3">
</td>
</tr>
<tr>
<td>
<label for="new_libelle" class="postionate">Libellé de salle</label>
</td>
<td>
<input type="text" size="50px" placeholder="Saisissez un nouveau libellé" class="ord" name="new_libelle" id="new_libelle"><br>
</td>
</tr>
</table>

</div>
<table class="content" >
<tr>
<td>
<input type="submit" name="submit" value="Valider">
</form>
</td>
<td>
<input  type="button" onclick="javascript:window.location.replace('salle_index.php')" value="Retourner">
</td>
</tr>
</table>
<?php
include 'ajouter_salle.php'; 
?>
<table class="media" align="left">
<tr>
<td><A href="https://www.youtube.com/channel/UCyFXZbYLiKOkthY26bCPn1Q"><img  src="../youtube.jpg" height="15px" width="30px"></A></td>
<td><A href="https://www.facebook.com/UniversityOUJDA/"><img  src="../facebook.jpg" height="15px" width="30px"></A></td>
<td><A href="http://esto.ump.ma/"><img  src="../esto.jpg" height="15px" width="30px"></A></td>
</tr>
</table>
<div class="copyright">Copyright © 2020 Université Mohammed Premier Oujda</div>
</body>
</html>