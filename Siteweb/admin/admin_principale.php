﻿<?php
session_start();
if(!isset($_SESSION['login']))
{
include 'connexion_admin_deconnexion.php';
header("location:connexion_admin.php");
exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Admin page </title>
    <style>
      
        *
{
    margin:0;
    padding:0;
   
  }
        body{
              background: linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)),url(background.jpg);
              background-size:cover;
              background-position:bottom;   
              }


	.menu img
{
    width: 50px;
    height: 60px;
    float: left;
    position: absolute;
    left: 41%;
    top: 18%;
}
.menu
{

	width: 100%;
	height: 76px;
	background-color: #fff;
	position: fixed;
	margin-top : -1%;
}
.scolarite
{
    font-family: 'Hind Vadodara',sans-serif;
    letter-spacing: 2px;
    position: absolute;
    left: 46%;
    top: 33%;
    font-size:37px ;
    text-decoration: none;
    color: #1e1e1e;
}
.deco
{
   font-family: 'Hind Vadodara',sans-serif;
    letter-spacing: 2px;
    position: absolute;
    left: 91%;
    top: 52%;
    font-size:12px ;
    text-decoration: none;
    color: #1e1e1e;
  
}

    .a1{
        position:absolute;
        top:30%;
        left:15%;
    }
	.a2{
        position:absolute;
        top:26%;
        left:35%;
    }
     .a3{
        position:absolute;
        top:30%;
        left:55%;
    }
	 .a4{
        position:absolute;
        top:30%;
        left:75%;
    }
	.a5{
        position:absolute;
        top:66%;
        left:15%;
    }
	.a6{
        position:absolute;
        top:66%;
        left:35%;
    }
       
      .text {
        color: white;
        font-size: 20px;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
		 font-family: 'Hind Vadodara',sans-serif;
    }
  
       .overlay {
        position: absolute;
		bottom:20%;
		left:10%;
        height: 70%;
        width: 80%;
        opacity: 0;
        transition: .5s ease;
        background-color:#93cfff91;
    }
	
	      .overlay1 {
        position: absolute;
		bottom:15%;
		left:10%;
        height: 70%;
        width: 80%;
        opacity: 0;
        transition: .5s ease;
        background-color:#93cfff91;
    }
	
         .overlay2 {
        position: absolute;
		bottom:5%;
        height: 96%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color:#93cfff91;
    }
	
	         .overlay3 {
        position: absolute;
		bottom:5%;
        height: 96%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color:#93cfff91;
    }
	
		         .overlay4 {
        position: absolute;
		bottom:5%;
        height: 100%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color:#93cfff91;
    }

		         .overlay5 {
        position: absolute;
		bottom:5%;
        height: 100%;
        width: 100%;
		text-align:center;
        opacity: 0;
        transition: .5s ease;
        background-color:#93cfff91;
    }
         


    .container:hover .overlay {
        opacity: 1;
		border-radius:15px;
    }
     .container:hover .overlay1 {
        opacity: 1;
		border-radius:15px;
    }
       .container:hover .overlay3 {
        opacity: 1;
		border-radius:15px;
    }
        .container:hover .overlay2 {
        opacity: 1;
		border-radius:15px;
    }
        .container:hover .overlay4 {
        opacity: 1;
		border-radius:15px;
    }
	    .container:hover .overlay5 {
        opacity: 1;
		border-radius:15px;
    }
	
      


      .image {
        display: block;
        width: 100%;
        height: auto;
    }
  
input[type="image"]
{
	display:inline;
	margin-top:1%;
	margin-right:98%;
	margin-left:2%;
	width:50px;
	height:50px;
}
       .title{
        position:absolute;
        top:12%;
        left:5%;
		width:800px;
    }
    </style>
	<link rel="icon" href="ump.png" type="image/x-icon" />
<script>
function startw(date)
  {
    var diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1);
  
    return new Date(date.setDate(diff));
 
  }
function mytoday()
{
		var result = new Date();
		result.setDate(result.getDate());
		var result = startw(result);
		let day = new Date(result).toISOString().slice(0, 10);
		location.replace('clean_data.php?monday='+day);
}
</script>
</head>
<body>
    <header>
        <nav class="menu">
            <a href="../index.php" class="scolarite">Scolarité</a>
            <img src="ump.png" alt="">
            <a href="connexion_admin_deconnexion.php" class="deco">Se déconnecter</a>
        </nav>
    </header>
	<a class="title">
<?php
if(isset($_REQUEST['clean']))
{
    echo "<h4 style='margin-left:5%;color:green;font-family:tahoma;'>Mise à jour de base de données réussie</h4>";
	 unset($_REQUEST['clean']);
}
?><br>
<?php
if(isset($_REQUEST['erreur']))
{
    echo "<h4 style='margin-left:5%;color:red;font-family:tahoma;'>Mise à jour de base de données erronée</h4>";
	 unset($_REQUEST['erreur']);
}
?><br>
	</a>
<a href="examen/examen_principale.php" class="a1">
        <div class="container">
            <img src="exam1.png" alt="exam1" class="image" style="width:180px;height:180px;">
            <div class="overlay2">
                <div class="text">Examens</div>
            </div>
        </div>
    </a>
    <a href="filiere/filiere_index.php" class="a2">
        <div class="container">
            <img src="filiere.png" alt="filiere" class="image" style="width:230px;height:230px;">
            <div class="overlay">
                <div class="text">Filières</div>
            </div>
        </div>
    </a>

    <a href="salle/salle_index.php" class="a3">
        <div class="container">
            <img src="salle.png" alt="filiere" class="image" style="width:170px;height:170px;">
            <div class="overlay2">
                <div class="text">Salles</div>
            </div>
        </div>
    </a>
	
	<a href="etab/etab_principale.php" class="a4">
        <div class="container">
            <img src="etab.png" alt="filiere" class="image" style="width:170px;height:170px;">
            <div class="overlay3">
                <div class="text">Etablissements</div>
            </div>
        </div>
    </a>
	
    <a href="module/module_index.php" class="a5">
        <div class="container">
            <img src="module.png" alt="module" class="image" style="width:170px;height:170px;">
            <div class="overlay4">
                <div class="text">Modules</div>
            </div>
        </div>
    </a>
	
    <a id="a6" href="#" onclick="mytoday()" class="a6">
        <div class="container">
            <img src="clean.png" alt="clean" class="image" style="width:170px;height:170px;">
            <div class="overlay5">
                <div class="text" >Mise à jour</div>
            </div>
        </div>
    </a>
	
</body>
</html>