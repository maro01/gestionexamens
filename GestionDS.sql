-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2020 at 09:45 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ste`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrateur`
--

CREATE TABLE `administrateur` (
  `id_admin` int(20) NOT NULL,
  `motdepasse` varchar(60) CHARACTER SET utf8 NOT NULL DEFAULT 'NOT NULL',
  `nom_utilisateur` varchar(60) CHARACTER SET utf8 NOT NULL DEFAULT 'NOT NULL'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrateur`
--

INSERT INTO `administrateur` (`id_admin`, `motdepasse`, `nom_utilisateur`) VALUES
(0, '$2y$10$No5vIBwtW/NY0c.Xg9mGMeJgZZ61FVmsNafUtYnnq7ry2aL9ayNQi', '$2y$10$No5vIBwtW/NY0c.Xg9mGMeJgZZ61FVmsNafUtYnnq7ry2aL9ayNQi');

-- --------------------------------------------------------

--
-- Table structure for table `etablissement`
--

CREATE TABLE `etablissement` (
  `id_etab` int(20) NOT NULL,
  `libelle_etab` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `etablissement`
--

INSERT INTO `etablissement` (`id_etab`, `libelle_etab`) VALUES
(1, 'FSJES'),
(2, 'FLSH'),
(3, 'FS');

-- --------------------------------------------------------

--
-- Table structure for table `etablissement_rat`
--

CREATE TABLE `etablissement_rat` (
  `id_etab_rat` int(20) NOT NULL,
  `libelle_etab_rat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `etablissement_rat`
--

INSERT INTO `etablissement_rat` (`id_etab_rat`, `libelle_etab_rat`) VALUES
(1, 'FSJES'),
(2, 'FLSH'),
(3, 'FS'),
(4, 'EST'),
(5, 'ENCG'),
(6, 'ENSA'),
(7, 'FMP'),
(8, 'CL'),
(9, 'CRI');

-- --------------------------------------------------------

--
-- Table structure for table `examen`
--

CREATE TABLE `examen` (
  `id_examen` int(20) NOT NULL,
  `date` date NOT NULL,
  `type_horaire` int(1) NOT NULL,
  `id_filiere` int(20) NOT NULL,
  `id_niveau` int(20) NOT NULL,
  `id_module` int(20) NOT NULL,
  `id_salle` int(20) NOT NULL,
  `id_etab` int(20) NOT NULL,
  `id_groupe` int(20) NOT NULL,
  `id_etab_rat` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `examen`
--

INSERT INTO `examen` (`id_examen`, `date`, `type_horaire`, `id_filiere`, `id_niveau`, `id_module`, `id_salle`, `id_etab`, `id_groupe`, `id_etab_rat`) VALUES
(33, '2020-05-12', 3, 27, 1, 4, 28, 1, 5, 4),
(35, '2020-05-11', 2, 45, 3, 2, 1, 1, 2, 1),
(38, '2020-05-15', 1, 72, 3, 4, 3, 3, 1, 1),
(42, '2020-05-11', 3, 72, 3, 4, 34, 3, 2, 5),
(43, '2020-05-11', 3, 72, 3, 4, 35, 3, 3, 5),
(44, '2020-05-12', 1, 63, 3, 1, 40, 2, 1, 6),
(45, '2020-05-13', 2, 71, 3, 1, 1, 2, 1, 1),
(46, '2020-05-12', 1, 28, 1, 5, 7, 2, 1, 1),
(47, '2020-06-01', 1, 20, 2, 4, 38, 3, 1, 6),
(48, '2020-06-01', 1, 28, 1, 1, 1, 2, 2, 1),
(49, '2020-06-01', 1, 28, 1, 1, 2, 2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `filiere`
--

CREATE TABLE `filiere` (
  `id_filiere` int(20) NOT NULL,
  `libelle_filiere` varchar(50) NOT NULL,
  `id_etab` int(20) NOT NULL,
  `id_niveau` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `filiere`
--

INSERT INTO `filiere` (`id_filiere`, `libelle_filiere`, `id_etab`, `id_niveau`) VALUES
(4, 'Licence Fondamentale : Droit  en langue française', 1, 2),
(5, 'Licence fondamentale : droit en langue arabe', 1, 2),
(6, 'Licence Fondamentale :  sciences économiqes et ges', 1, 2),
(7, 'Licence professionnelle : GESTION ET MEDIATION SOC', 1, 2),
(8, 'Licence Fondamentale en Géographie', 2, 2),
(9, 'Licence Fondamentale en Etudes Islamiques', 2, 2),
(10, 'Licence Fondamentale Etudes Françaises', 2, 2),
(11, 'Licence Fondamentale en Histoire et Civilistation', 2, 2),
(12, 'Licence Fondamentale en Etudes Arabes', 2, 2),
(13, 'Licence Sciences de l Education: Spécialité Langue', 2, 2),
(14, 'Licence Fondamentale Sociologie', 2, 2),
(15, 'Licence Fondamentale Etudes Anglaises', 2, 2),
(16, 'Licence Fondamentale en Etudes Amazighes', 2, 2),
(17, 'Licence Fondamentale Sciences de la Matière Physiq', 3, 2),
(18, 'Licence Fondamentale Sciences de la Matière Chimie', 3, 2),
(19, 'Licence Fondamentale Sciences de la Terre et de l ', 3, 2),
(20, 'Licence Fondamentale en Sciences Mathématiques', 3, 2),
(21, 'Licence Fondamentale Sciences Mathématiques et Inf', 3, 2),
(22, 'Licence Professionnelle  Environnement et Génie ci', 3, 2),
(23, 'Licence Fondamentale en Sciences de la  Vie', 3, 2),
(24, 'Licence Profess. Horticulture Ornementale et Espac', 3, 2),
(25, 'Licence Fondamentale : Droit  en langue française', 1, 1),
(26, 'Licence fondamentale : droit en langue arabe', 1, 1),
(27, 'Licence Fondamentale : sciences économiqes et gest', 1, 1),
(28, 'Licence Fondamentale en Géographie', 2, 1),
(29, 'Licence Fondamentale en Etudes Islamiques', 2, 1),
(30, 'Licence Fondamentale Etudes Françaises', 2, 1),
(31, 'Licence Fondamentale en Histoire et Civilistation', 2, 1),
(32, 'Licence Fondamentale en Etudes Arabes', 2, 1),
(33, 'Licence Sciences de l Education: Spécialité Langue', 2, 1),
(34, 'Licence Fondamentale Sociologie', 2, 1),
(35, 'Licence Fondamentale Etudes Anglaises', 2, 1),
(36, 'Licence Fondamentale en Etudes Amazighes', 2, 1),
(37, 'Licence Fondamentale Sciences de la Matière Physiq', 3, 1),
(38, 'Licence Fondamentale Sciences de la Matière Chimie', 3, 1),
(39, 'Licence Fondamentale Sciences de la Terre et de l ', 3, 1),
(40, 'Licence Fondamentale en Sciences Mathématiques', 3, 1),
(41, 'Licence Fondamentale Sciences Mathématiques et Inf', 3, 1),
(42, 'Licence Professionnelle  Environnement et Génie ci', 3, 1),
(43, 'Licence Fondamentale en Sciences de la  Vie', 3, 1),
(44, 'Licence Profess. Horticulture Ornementale et Espac', 3, 1),
(45, 'Master:Droit procédural et méthodes d exécution de', 1, 3),
(46, 'Master GESTION FINANCIERE ET FISCALE', 1, 3),
(47, 'Master : PNJ', 1, 3),
(48, 'Master Sécurité juridique des entreprises et des c', 1, 3),
(49, 'Master Spé Informatique Appliquée à l Economie et ', 1, 3),
(50, 'Master Spécialisé Sciences et techniques fiscales', 1, 3),
(51, 'Master Economie et Ingénierie Financière', 1, 3),
(52, 'Master : Conseils juridiques', 1, 3),
(53, 'Master Financement Banques et lnvestissement', 1, 3),
(54, 'Economie, Finance et Développement', 1, 3),
(55, 'Green Cultural Studies', 2, 3),
(56, 'Doctrine de Maliki: caractéristiques,origines et p', 2, 3),
(57, 'Relations financières', 2, 3),
(58, 'Master en MPMEI', 2, 3),
(59, 'Sciences du Langage et de la Traduction', 2, 3),
(60, 'Master en Géomatique et Gestion des Territoires', 2, 3),
(61, 'Ingénierie de la formation, Technologies Educative', 2, 3),
(62, 'Master Langue et Cultures Marocaines et Stratégie ', 2, 3),
(63, 'Master Littérature Générale et Comparée: Genres et', 2, 3),
(64, 'Master Etudes Linguistiques: questions et curricul', 2, 3),
(65, 'ÇáÊÚÏÏ ÇááÛæí æ ÇáÊáÇŞÍ ÇáËŞÇİí İí ÇáãÛÑÈ', 2, 3),
(66, 'Linguistique Générale et Didactique du FLE', 2, 3),
(67, 'Master en Géomatique et Gestion des Territoires', 2, 3),
(68, 'Master en Sciences du langage et Ingénierie pédago', 2, 3),
(69, 'Master Fiqh Al Mahjar: Osoloh Wa Qadayah Wa Tatbik', 2, 3),
(70, 'COMMUNICATION, CULTURE AND TRANSLATION', 2, 3),
(71, 'Littérature et Civilisation : Textes, Cultures et ', 2, 3),
(72, 'Master Chimie Appliquée', 3, 3),
(73, 'Master Analyse Appliquée', 3, 3),
(74, 'Master Analyse Fonctionnelle', 3, 3),
(75, 'Master Management Ressources Hydriques, Minières e', 3, 3),
(76, 'Master Environnement et Développement Durable', 3, 3),
(77, 'Master Physique de la Matière et de Rayonnement', 3, 3),
(78, 'Master Spécialisé en Ingénierie Informatique', 3, 3),
(79, 'Master Mécanique et Energétique', 3, 3),
(80, 'Master Analyse Numérique et Optimisation', 3, 3),
(81, 'Master spécialisé SAA', 3, 3),
(82, 'Master Physiologie Santé', 3, 3),
(83, 'Master Spécialisé INGENIERIE HORTICOLE ET PAYSAGER', 3, 3),
(84, 'Master Optique et Matériaux', 3, 3),
(85, 'Master Management Ressources Hydriques, Minières e', 3, 3),
(86, 'Master Statistique Probabilités et leurs Applicati', 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `groupe`
--

CREATE TABLE `groupe` (
  `id_groupe` int(20) NOT NULL,
  `libelle_groupe` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `groupe`
--

INSERT INTO `groupe` (`id_groupe`, `libelle_groupe`) VALUES
(1, 'Toute la classe'),
(2, 'Groupe 1'),
(3, 'Groupe 2'),
(4, 'Groupe 3'),
(5, 'Groupe 4'),
(6, 'Groupe 5'),
(7, 'Groupe 6'),
(8, 'Groupe 7'),
(9, 'Groupe 8'),
(10, 'Groupe 9'),
(11, 'Groupe 10');

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id_module` int(20) NOT NULL,
  `libelle_module` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id_module`, `libelle_module`) VALUES
(1, 'Module 1'),
(2, 'Module 2'),
(3, 'Module 3'),
(4, 'Module 4'),
(5, 'Module 5'),
(6, 'Module 6'),
(7, 'Module 7');

-- --------------------------------------------------------

--
-- Table structure for table `niveau`
--

CREATE TABLE `niveau` (
  `id_niveau` int(20) NOT NULL,
  `libelle_niveau` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `niveau`
--

INSERT INTO `niveau` (`id_niveau`, `libelle_niveau`) VALUES
(1, 'L2'),
(2, 'L3'),
(3, 'M2');

-- --------------------------------------------------------

--
-- Table structure for table `salle`
--

CREATE TABLE `salle` (
  `id_salle` int(20) NOT NULL,
  `id_etab_rat` int(20) NOT NULL,
  `libelle_salle` varchar(30) NOT NULL,
  `capacite` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `salle`
--

INSERT INTO `salle` (`id_salle`, `id_etab_rat`, `libelle_salle`, `capacite`) VALUES
(1, 1, 'Amphi A', 150),
(2, 1, 'Amphi B', 150),
(3, 1, 'Amphi C', 150),
(4, 1, 'Amphi D', 150),
(5, 1, 'Amphi E', 150),
(6, 1, 'Amphi F', 150),
(7, 1, 'Amphi G', 150),
(8, 2, 'Amphi 1', 150),
(9, 2, 'Amphi 2', 150),
(10, 2, 'Amphi 3', 150),
(11, 2, 'Amphi 4', 150),
(12, 3, 'Amphi A', 150),
(13, 3, 'Amphi B', 150),
(14, 3, 'Amphi C', 150),
(15, 3, 'Amphi D', 150),
(16, 3, 'Amphi E', 150),
(17, 3, 'Amphi F', 150),
(18, 3, 'Amphi G', 150),
(19, 3, 'Amphi H', 150),
(20, 3, 'Amphi I', 150),
(21, 3, 'Amphi J', 150),
(22, 3, 'Amphi K', 150),
(23, 3, 'Amphi L', 150),
(24, 4, 'D1', 70),
(25, 4, 'D2', 70),
(26, 4, 'D3', 70),
(27, 4, 'D4', 70),
(28, 4, 'D5', 70),
(29, 4, 'Amphi A', 150),
(30, 4, 'Amphi B', 150),
(31, 4, 'Amphi C', 150),
(32, 5, 'Classe A', 70),
(33, 5, 'Classe B', 70),
(34, 5, 'Classe C', 70),
(35, 5, 'Classe D', 70),
(36, 5, 'Classe E', 70),
(37, 6, 'Classe A', 70),
(38, 6, 'Classe B', 70),
(39, 6, 'Amphi A', 200),
(40, 6, 'Amphi B', 100),
(41, 6, 'Amphi C', 100),
(42, 7, 'Classe A', 70),
(43, 7, 'Classe B', 70),
(44, 7, 'Amphi 1', 200),
(45, 7, 'Amphi 2', 100),
(46, 7, 'Amphi 3', 200),
(47, 7, 'Amphi 4', 200),
(48, 7, 'Amphi 5', 200),
(49, 7, 'Amphi 6', 200),
(50, 7, 'Amphi 7', 200),
(51, 8, 'Classe 1', 50),
(52, 8, 'Classe 2', 50),
(53, 8, 'Classe 3', 50),
(54, 8, 'Amphi A', 100),
(55, 8, 'Amphi B', 100),
(56, 9, 'Classe 1', 50),
(57, 9, 'Amphi 1', 100);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrateur`
--
ALTER TABLE `administrateur`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `etablissement`
--
ALTER TABLE `etablissement`
  ADD PRIMARY KEY (`id_etab`);

--
-- Indexes for table `etablissement_rat`
--
ALTER TABLE `etablissement_rat`
  ADD PRIMARY KEY (`id_etab_rat`);

--
-- Indexes for table `examen`
--
ALTER TABLE `examen`
  ADD PRIMARY KEY (`id_examen`),
  ADD KEY `fk_filiere` (`id_filiere`),
  ADD KEY `fk_niveau` (`id_niveau`),
  ADD KEY `fk_module` (`id_module`),
  ADD KEY `fk_salle` (`id_salle`),
  ADD KEY `fk_etablissement` (`id_etab`),
  ADD KEY `fk_groupe` (`id_groupe`),
  ADD KEY `fk_etablissement_rat` (`id_etab_rat`);

--
-- Indexes for table `filiere`
--
ALTER TABLE `filiere`
  ADD PRIMARY KEY (`id_filiere`),
  ADD KEY `niveau_fk` (`id_niveau`),
  ADD KEY `etab_fk` (`id_etab`);

--
-- Indexes for table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`id_groupe`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id_module`);

--
-- Indexes for table `niveau`
--
ALTER TABLE `niveau`
  ADD PRIMARY KEY (`id_niveau`);

--
-- Indexes for table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`id_salle`),
  ADD KEY `fk_etab_ratt` (`id_etab_rat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `etablissement`
--
ALTER TABLE `etablissement`
  MODIFY `id_etab` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `etablissement_rat`
--
ALTER TABLE `etablissement_rat`
  MODIFY `id_etab_rat` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `examen`
--
ALTER TABLE `examen`
  MODIFY `id_examen` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `filiere`
--
ALTER TABLE `filiere`
  MODIFY `id_filiere` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `groupe`
--
ALTER TABLE `groupe`
  MODIFY `id_groupe` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id_module` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `niveau`
--
ALTER TABLE `niveau`
  MODIFY `id_niveau` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `salle`
--
ALTER TABLE `salle`
  MODIFY `id_salle` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `examen`
--
ALTER TABLE `examen`
  ADD CONSTRAINT `fk_etablissement_rat` FOREIGN KEY (`id_etab_rat`) REFERENCES `etablissement_rat` (`id_etab_rat`),
  ADD CONSTRAINT `fk_filiere` FOREIGN KEY (`id_filiere`) REFERENCES `filiere` (`id_filiere`),
  ADD CONSTRAINT `fk_groupe` FOREIGN KEY (`id_groupe`) REFERENCES `groupe` (`id_groupe`),
  ADD CONSTRAINT `fk_module` FOREIGN KEY (`id_module`) REFERENCES `module` (`id_module`),
  ADD CONSTRAINT `fk_niveau` FOREIGN KEY (`id_niveau`) REFERENCES `niveau` (`id_niveau`),
  ADD CONSTRAINT `fk_salle` FOREIGN KEY (`id_salle`) REFERENCES `salle` (`id_salle`);

--
-- Constraints for table `filiere`
--
ALTER TABLE `filiere`
  ADD CONSTRAINT `etab_fk` FOREIGN KEY (`id_etab`) REFERENCES `etablissement` (`id_etab`),
  ADD CONSTRAINT `niveau_fk` FOREIGN KEY (`id_niveau`) REFERENCES `niveau` (`id_niveau`);

--
-- Constraints for table `salle`
--
ALTER TABLE `salle`
  ADD CONSTRAINT `fk_etab_ratt` FOREIGN KEY (`id_etab_rat`) REFERENCES `etablissement_rat` (`id_etab_rat`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
