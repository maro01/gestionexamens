# gestionexamens

Cette application Web est pour objectif de gérer les examens universitaires de différents facultés universitaires avec possibilté de passer un examen d'une faculté dans une salle d'une autre faculté si les salles de la première faculté sont non vacants dans cette période (Exemple : passer un examen de faculté de medecine dans une salle d'une école d'ingénieurs ).

Cette application est utilisée par un administrateur et un etudiant.

pour accéder au compte administrateur , le login est :  root (Nom d'utilisateur) -- root (Mot de passe)

L'administrateur peut gérer toute la base de donnée du site web . L'étudiant ne peut que consulter l'emploi du temps des examens annoncées.
